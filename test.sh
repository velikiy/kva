#!/bin/sh
HOST="127.0.0.1:3000"
echo Initial check key1
curl -i $HOST/kv/key1
echo 
echo Put key/value1 with 4s expire
curl -H "X-Expire: 4" -i -X PUT -d value1 $HOST/kv/key1
echo 
echo Get non-expired key1
curl -i $HOST/kv/key1
sleep 6
echo 
echo Get expired key1
curl -i $HOST/kv/key1
echo 
