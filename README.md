# README #

### What is this repository for? ###

* This is a simple implementation of KV storage with expiration and REST access
* Erlang/OTP
* Uses [elli](https://github.com/knutin/elli) as HTTP server

### How do I get set up? ###

* Compiling and running debug console: __rebar3 shell__
* Configuration: __127.0.0.1:3000__ for HTTP access
* Dependencies: __[rebar3](https://s3.amazonaws.com/rebar3/rebar3), [elli](https://github.com/knutin/elli), [lager](https://github.com/basho/lager)__
* How to run tests: __./test.sh__
* Deployment instructions: __rebar3 as prod tar__