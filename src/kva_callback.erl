-module(kva_callback).
-export([handle/2, handle_event/3]).
-export([expire/0]).

-include_lib("elli/include/elli.hrl").
-behaviour(elli_handler).

handle(Req, _Args) ->
    handle(Req#req.method, elli_request:path(Req), Req).

handle('GET',[<<"kv">>, Key], _Req) ->
    %% Reply with a normal response. 'ok' can be used instead of '200'
    %% to signal success.
    lager:info("GET key: ~s", [Key]),
    case ets:lookup(kv, Key) of
      [] -> {404, [], <<"Not Found">>};
      [{Key, Value, _Time}] -> {ok, [], Value}
    end;

handle('PUT',[<<"kv">>, Key], Req) ->
    lager:info("PUT key: ~s",[Key]),
    Value=Req#req.body,
    case elli_request:get_header(<<"X-Expire">>, Req, unknown) of
      unknown -> Time=16#FFFFFFFF;
      Exp -> Time=erlang:system_time(seconds)+binary_to_integer(Exp)
    end,
    ets:insert(kv, {Key, Value, Time}),
    PrivDir = code:priv_dir(kva),
    ok=ets:tab2file(kv, PrivDir++"/kv.tab"),
    {ok, [], <<>>};

handle('POST', _, _Req) ->
    {405, [], <<"Method Not Allowed">>};

handle('DELETE', _, _Req) ->
    {405, [], <<"Method Not Allowed">>};

handle(_, _, _Req) ->
    {404, [], <<"Not Found">>}.

%% @doc: Handle request events, like request completed, exception
%% thrown, client timeout, etc. Must return 'ok'.
handle_event(_Event, _Data, _Args) ->
    ok.

expire() ->
    Time=erlang:system_time(seconds),
    Deleted=ets:select_delete(kv, [{{'$1', '$2', '$3'}, [{'<', '$3', Time}], [true]}]),
    case Deleted of
      0 -> ok;
      _ -> lager:info("Expired: ~b", [Deleted])
    end.
