-module(kva_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
    PrivDir = code:priv_dir(kva),
    case ets:file2tab(PrivDir++"/kv.tab") of
      {ok, _} ->
        lager:info("KV initialized:");
      {error, Reason} ->
        lager:warning("Initializing KV because: ~p", [Reason]),
        ets:new(kv, [set, public, named_table]),
        ok=ets:tab2file(kv, PrivDir++"/kv.tab")
    end,
    {ok, _TRef} = timer:apply_interval(1000, kva_callback, expire, []),
    kva_sup:start_link().

stop(_State) ->
    lager:info("Exiting app."),
    ok.
